
# **Vehicle Detection Project**
---

The goals / steps of this project are the following:

* Perform a Histogram of Oriented Gradients (HOG) feature extraction on a labeled training set of images and train a classifier Linear SVM classifier
* Optionally, you can also apply a color transform and append binned color features, as well as histograms of color, to your HOG feature vector. 
* Note: for those first two steps don't forget to normalize your features and randomize a selection for training and testing.
* Implement a sliding-window technique and use your trained classifier to search for vehicles in images.
* Run your pipeline on a video stream (start with the test_video.mp4 and later implement on full project_video.mp4) and create a heat map of recurring detections frame by frame to reject outliers and follow detected vehicles.
* Estimate a bounding box for vehicles detected.

[//]: # (Image References)
[car]: ./output_images/car_notcar.jpg
[features]: ./output_images/features.jpg
[tests]: ./output_images/tests.jpg
[video1]: ./project_video.mp4
[video2]: ./final_project_video.mp4

## [Rubric](https://review.udacity.com/#!/rubrics/513/view) Points
### Here I will consider the rubric points individually and describe how I addressed each point in my implementation.  

---

### Histogram of Oriented Gradients (HOG)

#### 1. Explain how (and identify where in your code) you extracted HOG features from the training images.

The code for this step is contained in the Section 1-3 of the IPython notebook called `VehDetection.ipynb`.  

I started by reading in all the `vehicle` and `non-vehicle` images from the dataset.  Here is an example of one of each of the `vehicle` and `non-vehicle` classes:

![alt text][car]

I then explored different color spaces and different `skimage.hog()` parameters (`orientations`, `pixels_per_cell`, and `cells_per_block`).  I grabbed random images from each of the two classes and displayed them to get a feel for what the `skimage.hog()` output looks like.

Here is an example using the `YCrCb` color space and HOG parameters of `orientations=9`, `pixels_per_cell=(8, 8)` and `cells_per_block=(2, 2)`:

![alt text][features]

#### 2. Explain how you settled on your final choice of HOG parameters.

I tried various combinations of parameters and found out the final combination. I also learned about how other people selected those parameters from the forum and the Facebook group, which is very helpful. The selection of the parameters is kind of a balance between accuracy and generalization capability. 

#### 3. Describe how (and identify where in your code) you trained a classifier using your selected HOG features (and color features if you used them).

I first of all extracted the features and converted them to vectors. Then I splitted the data randomly to training set and testing set. After that, I input the training data into a support vector machine and trained a model. The model was then applied to the testing set to validate its accuracy. It turned out to be as accurate as 99%. In this step, the most important thing is to get good features. At the very beginning, I tried different color spaces but did not get very good results until I swiched to YCrCb space. 

### Sliding Window Search

#### 1. Describe how (and identify where in your code) you implemented a sliding window search.  How did you decide what scales to search and how much to overlap windows?

Since only the lower part of the image is useful to find cars, I just used sliding windows in the lower part of the image. The size of the window is set to be roughly the same as the size of cars when they appear in the image. Since the size of a car could be totally different when it's at different positions of the image. I actually applied windows of different sizes to find cars. At the very beginning, I calculated HOG feature every time a new window was evaluated. Later on, I found that subsampling HOG from the HOG feature of the whole frame was much more efficient, so I switched to the new approach. I tried to tweak the parameters a little bit but it did not make too much difference. Eventually, I basically followed the parameters from the courses.


#### 2. Show some examples of test images to demonstrate how your pipeline is working.  What did you do to optimize the performance of your classifier?

At the beginning, I only used HOG features, but the results turned out to be unsatisfactory. So I tried to add other features like spatially binned color and histograms of color into the feature vector, which provided a nice result finally. I applied feature extraction to all of the channels. Here are some example images, which shows my classifier works well:

![alt text][tests]
---

### Video Implementation

#### 1. Provide a link to your final video output.  Your pipeline should perform reasonably well on the entire project video (somewhat wobbly or unstable bounding boxes are ok as long as you are identifying the vehicles most of the time with minimal false positives.)
Here's a [video2](./final_project_video.mp4)


#### 2. Describe how (and identify where in your code) you implemented some kind of filter for false positives and some method for combining overlapping bounding boxes.

I recorded the positions of positive detections in each frame of the video.  From the positive detections I created a heatmap. To reduce false positives, I then filtered that heatmap by a threshold. It turned out that it can effectively remove those false positives and identify vehicle positions.  I then used `scipy.ndimage.measurements.label()` to identify individual blobs in the heatmap.  I then assumed each blob corresponded to a vehicle.  I constructed bounding boxes to cover the area of each blob detected.  

You can check my result video to see the effect. Each yellow box in the video is considered to be a blob detected by `scipy.ndimage.measurements.label()`.

---

### Discussion

#### 1. Briefly discuss any problems / issues you faced in your implementation of this project.  Where will your pipeline likely fail?  What could you do to make it more robust?

One of the biggest problem is that it's quite difficult to detect those cars that are far away from the camera. Because of their sizes, the window generally cannot capture their 'useful' feature. 
Other problem is that, sometimes, cars from the opposite approach will be detected. This is understandable, because they are cars too. But in this project, we have to filter them out.
Also, when the environment changes, for example, pavement or lighting, the accuracy will be a little lower. 
To make it more robust, when training classifier, we should input more data (pictures with different lighting, for example) and tweak parameters for more time. We can also apply other classifiers, for example, CNN, to detect cars. 




```python

```
